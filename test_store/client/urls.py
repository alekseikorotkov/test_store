from django.urls import path
from . import views

urlpatterns = [
    path('client/', views.add_client, name='add_client'),
    path('client/<int:pk>/edit/',views.client_edit, name='client_edit'),
    path('', views.clients, name='clients'),
]

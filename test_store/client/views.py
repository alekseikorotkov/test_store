from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from .forms import ClientForm, FindForm
from .models import Client
# Create your views here.

def add_client(request):
    """add new client
    
    Arguments:
        request  -- request
    """
    if request.method == 'POST':
        form = ClientForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('clients')
    else:
        form = ClientForm()
    con = {
        'form': form
    }
    return render(request,'index.html',con)

def client_edit(request, pk):
    """edit data of client
    
    Arguments:
        request  -- request
        pk  -- [prymary key of client
    
    
    """
    client = get_object_or_404(Client, pk=pk)
    if request.method == "POST":
        form = ClientForm(request.POST, instance=client)
        if form.is_valid():
            client = form.save()
            client.save()
            return redirect('client_edit', pk=client.pk)
    else:
        form = ClientForm(instance=client)
    con = {
        'form': form
    }
    return render(request, 'index.html', con)
            
def clients(request):
    """ get list of clients
    
    Arguments:
        request  -- request
    
    Returns:
        render -- render page of list's clients 
    """
    if request.method == "POST":
        
        form = FindForm(request.POST)
        category = '{}__contains'.format(form['field_category'].value())
        value = form['field_find'].value()
        filer_by = {
            category:value
        }
        clients = Client.objects.filter(**filer_by)
        con = {
            'form':form,
            'clients': clients,
        }
        return render(request, 'list_client.html', con)

    clients = Client.objects.all()
    find_form = FindForm()
    con = {
        'form':find_form,
        'clients': clients,
    }
    return render(request, 'list_client.html', con)
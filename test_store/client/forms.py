from django import forms 
from client.models import Client

class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = '__all__' 

class FindForm(forms.Form):
    CHOICES = (('name', 'name'),('email', 'email'),('phone_number', 'phone number'), ('suburb', 'suburb'))
    field_category = forms.ChoiceField(choices=CHOICES)
    field_find = forms.CharField(required=False)
    
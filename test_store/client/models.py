from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.
class Client(models.Model):
    
    name = models.CharField(max_length=30, unique=True)
    steet_name = models.CharField(max_length=30, blank=True)
    suburb = models.CharField(max_length=30, blank=True)
    postcode = models.CharField(max_length=5, blank=True)
    state = models.CharField(max_length=30, blank=True)
    contact_name = models.CharField(max_length=30, blank=True)
    email = models.EmailField()
    phone_number = PhoneNumberField()
